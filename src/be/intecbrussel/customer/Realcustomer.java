package be.intecbrussel.customer;

public class Realcustomer extends AbstractCustomer {

	public Realcustomer(String name) {
		this.name = name;
	}

	public String getName() {
		return "Customer " + name;
	}

	public boolean isNil(boolean nil) {
		return false;
	}

}
