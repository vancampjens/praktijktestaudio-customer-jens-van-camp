package be.intecbrussel.customer;

public abstract class AbstractCustomer {

	protected String name;

	public abstract String getName();

	public abstract boolean isNil(boolean nil);

}
