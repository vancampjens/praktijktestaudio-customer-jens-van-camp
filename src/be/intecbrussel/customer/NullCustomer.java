package be.intecbrussel.customer;

public class NullCustomer extends AbstractCustomer {

	protected String name;

	public String getName() {
		return "Not available in Customer Database";
	}

	public boolean isNil(boolean nil) {
		return true;
	}

}
