package be.intecbrussel.audioplayer;

public class AudioPlayer implements MediaPlayer {

	final MediaAdapter mediaAdapter = new MediaAdapter();

	public void play(String audioType, String fileName) {
		if (audioType == "mp3") {
			System.out.println("Playing mp3 file. Name: " + fileName);
		} else if (audioType == "mp4") {
			System.out.println("Playing mp4 file. Name: " + fileName);
		} else if (audioType == "vlc") {
			System.out.println("Playing vlc file. Name: " + fileName);
		} else {
			System.out.println("Invalid media. " + audioType + " not supported");
		}

	}

}
