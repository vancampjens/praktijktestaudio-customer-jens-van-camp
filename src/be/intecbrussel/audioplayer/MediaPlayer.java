package be.intecbrussel.audioplayer;

public interface MediaPlayer {

	public void play(String audioType, String fileName);

}
