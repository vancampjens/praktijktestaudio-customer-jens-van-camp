package be.intecbrussel.audioplayer;

public interface AdvancedMediaPlayer {

	public void PlayVlc(String fileName);

	public void playMp4(String fileName);

}
